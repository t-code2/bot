/**
 * Validate users invite input
 * @param {string} limit User input
 * @returns {boolean} Valid or invalid user input
 */
function limitValidate(limit) {
  const limitNumber = parseInt(limit);

  if (!limitNumber) return false;
  if (limitNumber < 1 || limitNumber > 200) return false;

  return true;
}

/**
 * Validate speed invite users input
 * @param {string} speed User input
 * @returns {boolean} Valid or invalid user input
 */
function speedValidate(speed) {
  const speedNumber = parseInt(speed);

  if (!speedNumber) return false;
  if (speedNumber < 1 || speedNumber > 60) return false;

  return true;
}

/**
 * Check on exsist channel
 * @param {string} channelName Channel name
 * @returns {boolean} Exsist or not channel
 */
function checkExistingChannel(channelName) {
  // TODO: check existing of channel
  return false;
}

module.exports = {
  limitValidate,
  speedValidate,
  checkExistingChannel,
};
