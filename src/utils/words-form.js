/**
 * Get current word form minutes for "в \d <минут>" sentence
 * @param {number} minutesNumber Count minutes
 * @returns Correct word form
 */
function getMinutesWordForm(minutesNumber) {
  const decimal = Math.abs(minutesNumber) % 100;
  const lastDigit = decimal % 10;

  if (decimal > 10 && decimal < 20) return "минут";
  if (lastDigit > 1 && lastDigit < 5) return "минуты";
  if (lastDigit == 1) return "минуту";

  return "минут";
}

module.exports = {
  getMinutesWordForm,
};
