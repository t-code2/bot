const { default: axios } = require("axios");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const { EXCEL_MIME_TYPE } = require("./utils/mime-types");

const YANDEX_API_LINK =
  "https://cloud-api.yandex.net/v1/disk/public/resources/download?";

/**
 * Function for download file by public file
 * @param {string} publicUrl public url on file in yandex disk
 * @param {string} fileSaveFolder path to folder where download file
 * @returns {string} path to download file
 */
async function downloadFileFromYandex(publicUrl, fileSaveFolder) {
  const linkDownload = `${YANDEX_API_LINK}public_key=${publicUrl}`;
  const { data } = await axios.get(linkDownload);
  const res = await axios({
    url: data.href,
    method: "GET",
    responseType: "arraybuffer",
  });
  if (res.headers["content-type"] !== EXCEL_MIME_TYPE) {
    throw new Error("Загружен не excel файл");
  }

  const filePath = `${fileSaveFolder}\\file_${uuidv4()}.xlsx`;
  const fileStream = fs.createWriteStream(filePath);
  // write file in promise way
  await new Promise((resolve, reject) => {
    fileStream.write(res.data, (err) => {
      if (err) reject();

      fileStream.close();
      resolve();
    });
  });
  return filePath;
}

module.exports = {
  downloadFileFromYandex,
};
